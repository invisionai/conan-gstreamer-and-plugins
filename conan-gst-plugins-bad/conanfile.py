from conans import ConanFile, Meson, tools

import os
import glob
import shutil

class GStreamerPluginsGoodConan(ConanFile):
    name = "gst-plugins-bad"
    version = "1.16.0"
    default_user = "bincrafters"
    default_channel = "stable"
    url = "https://github.com/bincrafters/conan-" + name
    description = "Plug-ins is a set of plugins that we consider to have good quality code and correct functionality"
    license = "https://gitlab.freedesktop.org/gstreamer/gstreamer/raw/master/COPYING"
    settings = "os", "arch", "compiler", "build_type"
    options = {}
    default_options = tuple()

    folder_name = "gst-plugins-bad-" + version

    def requirements(self):
        self.requires("gstreamer/%s@%s/%s" % (self.version, self.user, self.channel))
        self.requires("gst-plugins-base/%s@%s/%s" % (self.version, self.user, self.channel))
        self.requires("ffmpeg/4.2.1@bincrafters/stable")

    def source(self):
        tools.get("https://github.com/GStreamer/gst-plugins-bad/archive/%s.tar.gz" % self.version)

    def _copy_pkg_config(self, name):
        root = self.deps_cpp_info[name].rootpath
        pc_dir = os.path.join(root, 'lib', 'pkgconfig')
        pc_files = glob.glob('%s/*.pc' % pc_dir)
        if not pc_files:  # zlib store .pc in root
            pc_files = glob.glob('%s/*.pc' % root)
        for pc_name in pc_files:
            new_pc = os.path.basename(pc_name)
            self.output.warn('copy .pc file %s' % os.path.basename(pc_name))
            shutil.copy(pc_name, new_pc)
            prefix = tools.unix_path(root) if self.settings.os == 'Windows' else root
            tools.replace_prefix_in_pc_file(new_pc, prefix)

    def build_requirements(self):
        if not tools.which("meson"):
            self.build_requires("meson_installer/0.50.0@bincrafters/stable")
        if not tools.which("pkg-config"):
            self.build_requires("pkg-config_installer/0.29.2@bincrafters/stable")
        self.build_requires("bison_installer/3.3.2@bincrafters/stable")
        self.build_requires("flex_installer/2.6.4@bincrafters/stable")

    def build(self):
        self._copy_pkg_config("gstreamer")
        self._copy_pkg_config("gst-plugins-base")
        self._copy_pkg_config("ffmpeg")

        # in Xavier there is an issue with cuda_gl_interop.h, we need to include <GL/gl.h> before
        # see https://forums.developer.nvidia.com/t/building-opencv-with-opengl-support/49741
        files_for_rep = glob.glob(f'{self.folder_name}/**/gstnv*enc.c', recursive=True)
        for fname in files_for_rep:
            print('Fixing CUDA GL INTEROP include in: ' + fname)
            tools.replace_in_file(fname, '#include <cuda_gl_interop.h>', "#include <GL/gl.h>\n#include <cuda_gl_interop.h>", strict=False)

        args = []
        args.append("-Dopenexr=disabled")

        meson = Meson(self)
        meson.configure(source_folder=self.folder_name, args=args)
        meson.build()
        meson.install()

    def package_info(self):
        self.cpp_info.libs = tools.collect_libs(self)
        self.cpp_info.srcdirs.append("src")
        self.env_info.GST_PLUGIN_PATH.append(os.path.join(self.package_folder, "lib", "gstreamer-1.0"))
        self.env_info.PKG_CONFIG_PATH.append(os.path.join(self.package_folder, "lib", "pkgconfig"))
        self.env_info.SOURCE_PATH.append(os.path.join(self.package_folder, "src"))

